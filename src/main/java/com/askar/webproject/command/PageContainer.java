package com.askar.webproject.command;

public class PageContainer {

    private PageContainer() {
    }

    public static final String INDEX_PAGE = "index.jsp";
    public static final String LOGIN_PAGE = "WEB-INF/views/login.jsp";
    public static final String REGISTER_PAGE = "WEB-INF/views/register.jsp";
    public static final String RESULT_REGISTER_PAGE = "WEB-INF/views/userInfo.jsp";
    public static final String ERROR_PAGE = "WEB-INF/views/error.jsp";
    public static final String USER_MENU_PAGE = "WEB-INF/views/usermenu.jsp";
    public static final String CART_PAGE = "WEB-INF/views/cart.jsp";
}
